const getSum = (str1, str2) => {
  if (
    typeof str1 === 'object' ||
    typeof str1 === 'number' ||
    typeof str2 === 'object' ||
    typeof str2 === 'number'
  ) {
    return false;
  }

  if (typeof str1 === 'string' && Number.isNaN(Number(str1))) {
    return false;
  }

  if (typeof str2 === 'string' && Number.isNaN(Number(str2))) {
    return false;
  }

  const sum = Number(str1) + Number(str2);

  return String(sum);
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;

  listOfPosts.forEach(post => {
    if (post.author === authorName) {
      posts += 1;
    }

    if (post?.comments) {
      post.comments.forEach(cmnt => {
        if (cmnt.author === authorName) {
          comments += 1;
        }
      });
    }
  });

  return `Post:${posts},comments:${comments}`;
};

const tickets = people => {
  const money = {
    25: 0,
    50: 0,
    100: 0
  };

  let changeWasGiven = false;

  for (let bill of people) {
    switch (Number(bill)) {
      case 25:
        money[25] += 1;
        break;

      case 50:
        money[50] += 1;
        break;

      case 100:
        money[100] += 1;
        break;

      default:
        break;
    }

    const change = Number(bill) - 25;

    changeWasGiven = false;

    switch (change) {
      case 0:
        changeWasGiven = true;
        break;

      case 25:
        if (money[25] > 0) {
          money[25] -= 1;
          changeWasGiven = true;
        }
        break;

      case 50:
        if (money[25] > 1) {
          money[25] -= 2;
          changeWasGiven = true;
        }
        if (money[50] > 0) {
          money[50] -= 1;
          changeWasGiven = true;
        }
        break;

      case 75:
        if (money[25] > 2) {
          money[25] -= 3;
          changeWasGiven = true;
        }
        if (money[25] > 0 && money[50] > 0) {
          money[25] -= 1;
          money[50] -= 1;
          changeWasGiven = true;
        }
        break;
    }

    if (!changeWasGiven) {
      return 'NO';
    }
  }

  return 'YES';
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
